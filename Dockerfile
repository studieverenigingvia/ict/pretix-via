FROM pretix/standalone:2025.2
USER root
RUN curl -sSL https://install.python-poetry.org | python -u - --version 1.6.1 && $HOME/.local/bin/poetry config virtualenvs.create false
# . is for the installing the pretix_via plugin.
COPY . /pretix_via
ENV DJANGO_SETTINGS_MODULE=
RUN cd /pretix_via \
    && make \
    && $HOME/.local/bin/poetry -C pretix-via-deposit install \
    && $HOME/.local/bin/poetry install \
    # TODO Remove if proper fix for SMTP relay available
    #  https://gitlab.com/studieverenigingvia/ict/pretix-via/-/issues/45
    && cat /pretix/src/production_settings.py /pretix_via/pretix_via/deployment/settings.py > /pretix/src/production_settings_via.py
USER pretixuser
ENV DJANGO_SETTINGS_MODULE=production_settings_via
RUN cd /pretix/src && make production
