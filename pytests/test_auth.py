import pytest

from pretix.base.models import User
from pretix.base.settings import GlobalSettingsObject


@pytest.fixture
def oauth_settings(organizer):
    gs = GlobalSettingsObject()
    gs.settings.via_oauth_client_id = "CLIENT_ID"
    gs.settings.via_oauth_client_secret = "CLIENT_SECRET"
    gs.settings.via_oauth_organizer = organizer.pk
    gs.settings.flush()


@pytest.fixture
def remote_via_user(requests_mocker):
    requests_mocker.post(
        "https://svia.nl/oauth/token",
        json={
            "access_token": "some_access_token",
            "refresh_token": "some_refresh_token",
            "expires_in": 864000,
            "token_type": "Bearer",
        },
    )

    requests_mocker.post(
        "https://svia.nl/oauth/introspect",
        json={
            "active": True,
            "aud": "public",
            "client_id": "public",
            "exp": 1536852643,
            "iat": 1535988643,
            "iss": "https://svia.nl/",
            "scope": "pimpy",
            "sub": 69,  # Maico
            "token_type": "Bearer",
            "username": "john.doe@mail.com",
            "full_name": "John Doe",
        },
    )

    requests_mocker.get(
        "https://svia.nl/api/users/self/",
        json={
            "id": 69,
            "email": "john.doe@mail.com",
            "disabled": "False",
            "first_name": "John",
            "last_name": "Doe",
            "student_id": "123456789",
            "birth_date": "1990-01-01",
            "phone_nr": "0624705415",
            "locale": "en",
            "study_start": "1899-12-31",
            "educations": [
                {
                    "id": 11,
                    "nl_name": "Artificial Intelligence",
                    "en_name": "Artificial Intelligence",
                }
            ],
            "tfa_enabled": True,
            "address": "Some street",
            "zip": "1000aa",
            "city": "Amsterdam",
            "country": "Nederland",
            "member": True,
            "paid_date": "2019-10-03",
            "honorary_member": False,
            "favourer": False,
            "iban": "",
        },
    )

    requests_mocker.get(
        "https://svia.nl/api/users/self/groups/",
        json=[
            {
                "id": 1,
                "name": "ICT-commissie",
                "maillist": "ict",
                "mailtype": "mailinglist",
            },
        ],
    )


@pytest.mark.django_db
@pytest.mark.usefixtures("oauth_settings", "remote_via_user")
def test_auth_no_existing_user(
    client,
):
    rv = client.get("/control/login")
    assert b"Login with via" in rv.content, rv.content
    assert b"https://svia.nl/oauth/authorize" in rv.content

    rv = client.get(
        "/control/login?code=76uFNyRVHRclncz5qPuXzqlEZG6pccNj50xhCc0FHDWTyuzs&state=%2Fcontrol%2F"
    )
    assert rv.status_code == 302, rv.content
    assert rv.url == "/control/"

    user = User.objects.get(auth_backend_identifier="69")
    assert user is not None
    assert user.fullname == "John Doe"
    assert user.auth_backend == "via-oauth"
    assert user.email == "john.doe@mail.com"

    assert len(user.teams.all()) == 1
    assert user.teams.all()[0].name == "ICT-commissie"
