from django.conf import settings

from pretix.base.plugins import get_all_plugins


def test_plugin_installed():
    assert "pretix_via" in settings.INSTALLED_APPS


def test_plugin_available():
    plugins = get_all_plugins()
    assert "pretix_via" in [plugin.app.name for plugin in plugins]
