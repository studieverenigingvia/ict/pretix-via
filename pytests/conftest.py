import pytest
import requests_mock
from rest_framework.test import APIClient

from pretix.base.models import Team, User, Organizer


@pytest.fixture(autouse=True, scope="function")
def requests_mocker():
    with requests_mock.Mocker() as m:
        yield m


@pytest.fixture
def organizer():
    return Organizer.objects.create(name="Studievereniging via", slug="via")


@pytest.fixture
def admin_team(organizer):
    return Team.objects.create(
        organizer=organizer,
        # TODO More permissions?
        can_view_orders=True,
        can_change_teams=True,
        name="Admin team",
        all_events=True,
        can_create_events=True,
    )


@pytest.fixture
def admin_user(admin_team):
    u = User.objects.create_superuser("maico@svia.nl", "dummy")
    admin_team.members.add(u)
    return u


@pytest.fixture
def client():
    return APIClient()
