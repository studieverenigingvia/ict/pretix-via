from typing import Any, Dict

import pytest

from pretix_via.settings import DEFAULTS
from pretix.base.settings import DEFAULTS as pretix_defaults


@pytest.mark.parametrize("key,value", DEFAULTS.items())
def test_settings_consistent(key: str, value: Dict[str, Any]):
    if key not in pretix_defaults:
        pytest.skip("Setting does not overwrite default pretix setting")
    assert value["type"] == pretix_defaults[key]["type"]
