import csv
import datetime
import io

import pytest
from django.utils.timezone import now
from django_scopes import scopes_disabled

from pretix.base.models import (
    Event,
    Quota,
    Item,
    Order,
    Question,
    OrderPosition,
    QuestionAnswer,
    SalesChannel,
)


@pytest.fixture
@scopes_disabled()
def event(organizer):
    alv = Event.objects.create(
        organizer=organizer,
        name="ALV",
        slug="alv",
        date_from=datetime.datetime(
            now().year + 1, 12, 26, tzinfo=datetime.timezone.utc
        ),
        plugins=",".join(
            [
                "pretix.plugins.sendmail",
                "pretix.plugins.statistics",
                "pretix.plugins.checkinlists",
                "pretix.plugins.autocheckin",
                "pretix_via",
                "pretix.plugins.ticketoutputpdf",
                "pretix_pages",
                "pretix.plugins.reports",
                "pretix.plugins.pretixdroid",
                "pretix_mollie",
            ]
        ),
        live=True,
    )

    alv.settings.set("timezone", "UTC")
    alv.settings.set("attendee_names_asked", True)
    return alv


@pytest.fixture
@scopes_disabled()
def quota(event):
    return Quota.objects.create(event=event, name="Tickets", size=5)


@pytest.fixture
@scopes_disabled()
def authorizing_ticket(event, quota):
    ticket = Item.objects.create(
        event=event,
        name="Early-bird ticket",
        category=None,
        default_price=0,
        admission=True,
    )
    quota.items.add(ticket)
    return ticket


@pytest.fixture
@scopes_disabled()
def attending_ticket(event, quota):
    ticket = Item.objects.create(
        event=event,
        name="Authorizing product",
        category=None,
        default_price=0,
        admission=True,
    )
    quota.items.add(ticket)
    return ticket


@pytest.fixture
@scopes_disabled()
def authorizing_name_question(event, authorizing_ticket):
    q = Question.objects.create(
        question="Authorizing name",
        type=Question.TYPE_STRING,
        event=event,
        required=True,
    )
    authorizing_ticket.questions.add(q)
    return q


@pytest.fixture
@scopes_disabled()
def authorizing_email_question(event, authorizing_ticket):
    q = Question.objects.create(
        question="Authorized email",
        type=Question.TYPE_STRING,
        event=event,
        required=True,
    )
    authorizing_ticket.questions.add(q)
    return q


@pytest.fixture
@scopes_disabled()
def order_attending(
    event, attending_ticket, authorizing_email_question, authorizing_name_question
):
    o = Order.objects.create(
        code="ABCDE",
        event=event,
        status=Order.STATUS_PAID,
        datetime=now(),
        expires=now() + datetime.timedelta(days=10),
        sales_channel=SalesChannel.objects.first(),
        total=0,
    )
    OrderPosition.objects.create(
        order=o,
        positionid=1,
        item=attending_ticket,
        variation=None,
        price=0,
        attendee_name_parts={"full_name": "Maico 'present' Timmerman"},
        attendee_email="maico+present@svia.nl",
        secret="z3fsn8jyufm5kpk768q69gkbyr5f4h6w",
        pseudonymization_id="ABCDEFGHKL",
    )
    return o


@pytest.fixture
@scopes_disabled()
def order_authorizing(
    event, authorizing_ticket, authorizing_name_question, authorizing_email_question
):

    o = Order.objects.create(
        code="FGHIJ",
        event=event,
        status=Order.STATUS_PAID,
        datetime=now(),
        expires=now() + datetime.timedelta(days=10),
        sales_channel=SalesChannel.objects.first(),
        total=0,
    )
    op = OrderPosition.objects.create(
        order=o,
        positionid=1,
        item=authorizing_ticket,
        variation=None,
        price=0,
        attendee_name_parts={"full_name": "Maico 'absent' Timmerman"},
        attendee_email="maico+absert@svia.nl",
        secret="589d331f424d6685085810ad68d6d2ff",
        pseudonymization_id="MNOPQRSTUV",
    )
    QuestionAnswer.objects.create(
        orderposition=op,
        question=authorizing_name_question,
        answer="Maico 'auth' Timmerman",
    )
    QuestionAnswer.objects.create(
        orderposition=op,
        question=authorizing_email_question,
        answer="maico@svia.nl",
    )
    return o


@pytest.mark.django_db
def test_helios_export(
    organizer,
    event,
    authorizing_ticket,
    authorizing_name_question,
    authorizing_email_question,
    client,
    order_authorizing,
    order_attending,
    admin_user,
):
    assert client.login(email=admin_user.email, password="dummy")

    rv = client.post(
        f"/control/event/{organizer.slug}/{event.slug}/orders/export/do",
        {
            "exporter": "helios",
            "helios-_format": "default",  # CSV with commas
            "helios-authorizing": authorizing_ticket.id,
            "helios-authorizing_name": authorizing_name_question.id,
            "helios-authorizing_email": authorizing_email_question.id,
        },
    )
    assert rv.status_code == 302, rv.content
    assert rv.url.startswith("/download/")

    rv = client.get(rv.url)
    assert rv.status_code == 200, rv.content

    reader = csv.reader(io.StringIO(b"".join(rv.streaming_content).decode()))
    content = list(reader)
    assert content == [
        [
            "password",
            "FGHIJ",
            "maico@svia.nl",
            "Maico 'auth' Timmerman authorized by Maico 'absent' Timmerman",
        ],
        ["password", "ABCDE", "maico+present@svia.nl", "Maico 'present' Timmerman"],
    ]
