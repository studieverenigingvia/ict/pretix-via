import pytest

from pretix_via.signals.checkout import internationalize_phone_nr


@pytest.mark.parametrize(
    "phone,expected",
    [
        ("061234567", "+3161234567"),
        ("0612345678", "+31612345678"),
        ("06-12345678", "+31612345678"),
        ("+31612345678", "+31612345678"),
        ("0031612345678", "+31612345678"),
        ("+31 6 12345678", "+31612345678"),
    ],
)
def test_internationalize_phone_nr(phone, expected):
    assert internationalize_phone_nr(phone) == expected
