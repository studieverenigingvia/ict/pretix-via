from pretix.testutils.settings import *  # NOQA

PRETIX_AUTH_BACKENDS = ["pretix_via.auth.ViaOAuthBackend"]
