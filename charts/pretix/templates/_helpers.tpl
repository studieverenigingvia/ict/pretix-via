{{/*
Expand the name of the chart.
*/}}
{{- define "pretix.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "pretix.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "pretix.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "pretix.labels" -}}
helm.sh/chart: {{ include "pretix.chart" . }}
{{ include "pretix.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "pretix.selectorLabels" -}}
app.kubernetes.io/name: {{ include "pretix.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}


{{- define "pretix.databaseDsn" -}}
postgresql+psycopg2://{{ .Values.postgresql.auth.username }}:{{ .Values.postgresql.auth.password }}@{{ (include "pretix.fullname" .) }}-postgresql/{{ .Values.postgresql.auth.database }}
{{- end -}}

{{- define "pretix.brokerDsn" -}}
redis://:{{ .Values.redis.auth.password }}@{{ (include "pretix.fullname" .) }}-redis-headless
{{- end -}}


{{/*
Create the environment variables used by the Python app
*/}}
{{- define "pretix.appEnvironment" }}
  - name: NUM_WORKERS
    value: "2"
  - name: MOLLIE_API_KEY
    valueFrom:
         secretKeyRef:
            name: pretix-mollie
            key: api_key
{{- end }}
