import logging
from urllib.parse import urljoin

import requests

_logger = logging.getLogger(__name__)


def get_via_user_details(host, client_id, client_secret, viaduct_url):
    token_response = requests.post(
        urljoin(host, "/oauth/token?grant_type=client_credentials"),
        data={
            "client_id": client_id,
            "client_secret": client_secret,
            "scope": "pretix",
        },
    )
    if token_response.status_code != 200:
        _logger.error(
            "VIA OAuth client credentials failed:" + str(token_response.json())
        )
        return {}
    access_token = token_response.json()["access_token"]
    user_info_response = requests.get(
        viaduct_url, headers={"Authorization": f"Bearer {access_token}"}
    )
    if user_info_response.status_code != 200:
        _logger.error(
            "VIA user info retrieval failed:" + str(user_info_response.json())
        )
        return {}

    return user_info_response.json()
