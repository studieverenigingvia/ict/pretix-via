from django.conf import settings
from django.core.mail.backends.smtp import EmailBackend


class SMTPRelayFixEmailBackend(EmailBackend):
    """
    Custom fix for https://gitlab.com/studieverenigingvia/ict/pretix-via/-/issues/45
    """

    def open(self):
        """
        Ensure an open connection to the email server. Return whether or not a
        new connection was required (True or False) or None if an exception
        passed silently.
        """
        if self.connection:
            # Nothing to do if the connection is already open.
            return False

        # If local_hostname is not specified, socket.getfqdn() gets used.
        # For performance, we use the cached FQDN for local_hostname.

        # NOTE: This line is overwritten with a custom setting
        connection_params = {"local_hostname": settings.EMAIL_FQDN}
        if self.timeout is not None:
            connection_params["timeout"] = self.timeout
        if self.use_ssl:
            connection_params.update(
                {
                    "keyfile": self.ssl_keyfile,
                    "certfile": self.ssl_certfile,
                }
            )
        try:
            self.connection = self.connection_class(
                self.host, self.port, **connection_params
            )

            # TLS/SSL are mutually exclusive, so only attempt TLS over
            # non-secure connections.
            if not self.use_ssl and self.use_tls:
                self.connection.starttls(
                    keyfile=self.ssl_keyfile, certfile=self.ssl_certfile
                )
            if self.username and self.password:
                self.connection.login(self.username, self.password)
            return True
        except OSError:
            if not self.fail_silently:
                raise
