from django.contrib import messages
from django.db import transaction
from django.shortcuts import redirect

from pretix.base.settings import GlobalSettingsObject
from pretix.presale.forms.waitinglist import WaitingListForm
from pretix_via import api
from pretix_via.models import WaitingListEntryViaUser


class ViaUserAwareWaitingListForm(WaitingListForm):
    def __init__(self, *args, **kwargs):
        self.via_user = kwargs.pop("via_user", None)

        if self.via_user:
            kwargs["initial"]["email"] = self.via_user.get("email")

        super().__init__(*args, **kwargs)
        if self.plugin_enabled and self.via_user:
            self.fields["email"].widget.attrs["readonly"] = "readonly"
            self.via_user_instance = WaitingListEntryViaUser(
                user_id=self.via_user.get("id"),
                email=self.via_user.get("email"),
                address=self.via_user.get("address"),
                city=self.via_user.get("city"),
                zip=self.via_user.get("zip"),
                member=self.via_user.get("member"),
                favourer=self.via_user.get("favourer"),
                first_name=self.via_user.get("first_name"),
                last_name=self.via_user.get("last_name"),
                birth_date=self.via_user.get("birth_date"),
            )

    def save(self, commit=True):
        with transaction.atomic():
            instance = super().save(commit)
            if self.plugin_enabled and self.via_user:
                self.via_user_instance.waitinglistentry = instance
                self.via_user_instance.save()
            return instance

    @property
    def plugin_enabled(self):
        return "pretix_via" in self.event.get_plugins()

    @staticmethod
    def get_form_kwargs_decorator(f):
        def decorator(self, **kwargs):
            from pretix.presale.views.cart import cart_session

            kwargs = f(self, **kwargs)

            gs = GlobalSettingsObject()
            client_id = gs.settings.get("via_oauth_client_id")
            client_secret = gs.settings.get("via_oauth_client_secret")
            host = gs.settings.get("via_oauth_host", "https://svia.nl/")
            viaduct_url = (
                cart_session(self.request).get("widget_data", {}).get("user-info-url")
            )
            if viaduct_url:
                kwargs["via_user"] = api.get_via_user_details(
                    host, client_id, client_secret, viaduct_url
                )
            return kwargs

        return decorator

    @staticmethod
    def form_valid_decorator(f):
        def decorator(self, form):
            if not form.via_user and form.plugin_enabled:
                messages.error(
                    self.request, "You can only use this shop on the via-site."
                )
                return redirect(self.get_index_url())
            return f(self, form)

        return decorator
