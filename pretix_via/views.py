import json
import logging

from django.contrib import messages
from django.db import IntegrityError, transaction
from django.forms import formset_factory
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views import View
from django.views.generic import FormView, TemplateView
from django_filters.rest_framework import DjangoFilterBackend
from i18nfield.strings import LazyI18nString
from rest_framework import mixins, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.filters import OrderingFilter
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from pretix.base.forms import SafeSessionWizardView
from pretix.base.models import Event, Order, Team
from pretix.base.templatetags.money import money_filter
from pretix.control.permissions import EventPermissionRequiredMixin
from pretix.control.views.event import EventSettingsViewMixin
from pretix_via.models import OrderViaUser, TeamViaGroup
from pretix_via.serializers import (
    OrderSerializer,
    OrderViaUserSerializer,
    TeamViaGroupSerializer,
)
from .filters import OrderFilter
from .forms import (
    EventViaSettingsForm,
    EventViaTermsForm,
    EventViaTermsQuestionsForm,
    BaseEventViaTermsQuestionsFormSet,
    EventViaTermsAdditionalForm,
    BaseEventViaTermsAdditionalFormSet,
)

logger = logging.getLogger(__name__)


class EventViaSettings(EventSettingsViewMixin, EventPermissionRequiredMixin, FormView):
    model = Event
    form_class = EventViaSettingsForm
    context_object_name = "event"
    permission = "can_change_event_settings"

    template_name = "pretix_via/control_event_via_settings.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["obj"] = self.request.event
        return kwargs

    def get_success_url(self):
        return reverse(
            "plugins:pretix_via:event.settings.via",
            kwargs={
                "organizer": self.request.event.organizer.slug,
                "event": self.request.event.slug,
            },
        )

    def form_invalid(self, form):
        messages.error(
            self.request, _("We could not save your changes. See below for details.")
        )
        return super().form_invalid(form)

    def form_valid(self, form):
        form.save()
        messages.success(self.request, _("Your changes have been saved."))
        return redirect(self.get_success_url())


class EventViaTermsSettings(
    SafeSessionWizardView, EventSettingsViewMixin, EventPermissionRequiredMixin
):
    form_list = [
        ("terms", EventViaTermsForm),
        (
            "questions",
            formset_factory(
                EventViaTermsQuestionsForm,
                formset=BaseEventViaTermsQuestionsFormSet,
                can_order=False,
                can_delete=False,
                extra=0,
            ),
        ),
        (
            "additional",
            formset_factory(
                EventViaTermsAdditionalForm,
                formset=BaseEventViaTermsAdditionalFormSet,
                can_order=True,
                can_delete=True,
                extra=0,
            ),
        ),
    ]

    condition_dict = {
        "questions": lambda view: bool(view.request.event.questions.all())
    }

    templates = {
        "terms": "pretix_via/control_event_via_terms_common.html",
        "questions": "pretix_via/control_event_via_terms_questions.html",
        "additional": "pretix_via/control_event_via_terms_additional.html",
    }
    permission = "can_change_event_settings"

    def get_template_names(self):
        return [self.templates[self.steps.current]]

    def get_form_kwargs(self, step=None):
        kwargs = super().get_form_kwargs()
        kwargs["obj"] = self.request.event
        # We need to add kwargs for forms in formset to only show event locales in widget.
        if step in ("questions", "additional"):
            kwargs["form_kwargs"] = {"obj": self.request.event}
        return kwargs

    def get_form_initial(self, step):
        initial = super().get_form_initial(step)
        if step == "questions":
            initial = []
            current_questions = self.request.event.questions.all()
            initial_question_terms = json.loads(
                self.request.event.settings.questions_terms
            )

            for question in current_questions:
                if str(question.pk) in initial_question_terms:
                    initial.append(
                        {
                            "question": question.pk,
                            **initial_question_terms[str(question.pk)],
                        }
                    )
                else:
                    initial.append({"question": question.pk, "description": ""})
        elif step == "additional":
            additional_terms = self.request.event.settings.additional_terms
            initial = [{"term": LazyI18nString(t)} for t in additional_terms]
        return initial

    def get_context_data(self, form, **kwargs):
        ctx = super().get_context_data(form, **kwargs)
        if ctx["wizard"]["steps"].current == "terms":
            if self.request.event.settings.payment_term_last:
                ctx[
                    "payment_term_last"
                ] = self.request.event.settings.payment_term_last.datetime(
                    self.request.event
                )
            if self.request.event.settings.cancel_allow_user_paid_until:
                ctx[
                    "cancel_allow_user_paid_until"
                ] = self.request.event.settings.cancel_allow_user_paid_until.datetime(
                    self.request.event
                )
        if ctx["wizard"]["steps"].current == "questions":
            ctx["questions"] = zip(self.request.event.questions.all(), ctx["form"])
        return ctx

    def done(self, form_list, **kwargs):
        for form in form_list:
            form.save()
        self.request.event.settings.set("via_terms_enabled", True)
        self.request.event.cache.clear()
        messages.success(
            self.request, _("Terms and conditions have been saved and enabled.")
        )
        return redirect(
            reverse(
                "plugins:pretix_via:event.settings.via_terms",
                kwargs={
                    "organizer": self.request.event.organizer.slug,
                    "event": self.request.event.slug,
                },
            )
        )


class ViaUserOrdersListView(mixins.ListModelMixin, viewsets.GenericViewSet):
    model = OrderViaUser
    serializer_class = OrderSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filterset_class = OrderFilter
    permission = "can_view_orders"

    def get_queryset(self):
        return Order.objects.select_related("event", "via_user")

    def list(self, request, *args, **kwargs):
        via_user = kwargs.pop("via_user")
        queryset = self.get_queryset().filter(via_user__user_id=via_user)
        queryset = self.filter_queryset(queryset)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ViaUserEventOrdersListView(mixins.ListModelMixin, viewsets.GenericViewSet):
    model = OrderViaUser
    serializer_class = OrderSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filterset_class = OrderFilter
    permission = "can_view_orders"

    def get_queryset(self):
        return self.request.event.orders.select_related("via_user")

    def list(self, request, *args, **kwargs):
        via_user = kwargs.pop("via_user")
        queryset = self.get_queryset().filter(via_user__user_id=via_user)
        queryset = self.filter_queryset(queryset)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class OrderViaUserRetrieveAPIView(viewsets.GenericViewSet):
    ordering = ("datetime",)
    ordering_fields = ("datetime", "code", "status", "last_modified")
    lookup_field = "code"
    permission = "can_view_orders"
    serializer_class = OrderViaUserSerializer

    def get_queryset(self):
        return Order.objects.all()

    @action(detail=True, methods=["GET", "PUT"])
    def via_user(self, request, **kwargs):
        if request.method == "GET":
            return self.get_via_user(request, **kwargs)
        elif request.method == "PUT":
            return self.put_via_user(request, **kwargs)

    def get_via_user(self, request, **kwargs):
        order = super().get_object()
        order_via_user = get_object_or_404(OrderViaUser, order_id=order.pk)
        return Response(self.get_serializer(order_via_user).data)

    def put_via_user(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        order = super().get_object()
        with transaction.atomic():
            try:
                order.via_user.delete()
            except OrderViaUser.DoesNotExist:
                pass
            new_user = OrderViaUser(order=order, **serializer.validated_data)
            new_user.save()
            order.via_user = new_user
            order.save()
        return Response(self.get_serializer(order.via_user).data)


class TeamViaGroupListView(viewsets.ModelViewSet):
    ordering = ("team",)
    lookup_field = "group_id"
    permission = "can_change_teams"
    serializer_class = TeamViaGroupSerializer
    queryset = TeamViaGroup.objects.all()


class TeamViaGroupUpdateAPIView(viewsets.GenericViewSet):
    permission = "can_change_teams"
    serializer_class = TeamViaGroupSerializer
    queryset = Team.objects.all()

    @action(detail=True, methods=["PUT"])
    def via_group(self, request, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        team = super().get_object()

        with transaction.atomic():
            try:
                try:
                    team.via_group.delete()
                except TeamViaGroup.DoesNotExist:
                    pass
                new_group = TeamViaGroup(team=team, **serializer.validated_data)
                new_group.save()
                team.via_group = new_group
                team.via_group.save()
                team.save()
            except IntegrityError as e:
                raise ValidationError(str(e))
        return Response(self.get_serializer(team.via_group).data)


class TermsConditionsView(TemplateView):
    template_name = "pretix_via/event_terms_and_conditions.html"

    def get_context_data(self, **kwargs):
        event = self.request.event
        if not self.request.event.settings.via_terms_enabled:
            raise Http404(_("The requested page is not available."))

        ctx = super().get_context_data()

        # Initial quotas.
        quotas = event.quotas.filter(id__in=event.settings.availability_quotas).all()
        ctx["availability_show"] = True
        initial_availability = 0
        for quota in quotas:
            if quota.size is None:
                ctx["availability_show"] = False
            else:
                initial_availability += quota.size
        ctx["availability_initial"] = (
            initial_availability - event.settings.availability_reserved
        )

        ctx["favourer_allowed"] = event.settings.get("via_favourer_allowed")

        # Products and costs
        products = event.items.all()
        ctx["payment_available"] = sum(p.default_price for p in products) != 0
        if event.settings.cancel_allow_user_paid_until:
            ctx[
                "cancel_allow_user_paid_until"
            ] = event.settings.cancel_allow_user_paid_until.datetime(event)
        else:
            ctx["cancel_allow_user_paid_until"] = event.date_from

        if event.settings.payment_term_last:
            ctx["payment_term_last"] = event.settings.payment_term_last.datetime(event)
        else:
            ctx["payment_term_last"] = None
        if event.settings.replacement_until:
            ctx["replacement_until"] = event.settings.replacement_until.datetime(event)
        else:
            ctx["replacement_until"] = None

        if event.settings.payment_term_days:
            ctx["payment_term_days"] = event.settings.payment_term_days
        else:
            ctx["payment_term_days"] = None

        if event.settings.payment_term_minutes:
            ctx["payment_term_minutes"] = event.settings.payment_term_minutes
        else:
            ctx["payment_term_minutes"] = None

        if event.settings.no_show_fee:
            ctx["no_show_fee"] = money_filter(
                event.settings.no_show_fee, event.currency
            )
        else:
            ctx["no_show_fee"] = None

        if event.settings.contact_mail:
            ctx["contact_mail"] = event.settings.contact_mail
        else:
            ctx["contact_mail"] = "bestuur@svia.nl"

        ctx["questions_goals"] = []
        ctx["questions_externally_shared_with"] = []
        question_terms = json.loads(event.settings.questions_terms)
        for question in event.questions.all():
            if str(question.pk) not in question_terms:
                continue

            question_term = question_terms[str(question.pk)]
            ctx["questions_goals"].append(
                (question, LazyI18nString(question_term["goal"]))
            )
            externally_shared_with = LazyI18nString(
                question_term.get("externally_shared_with", "")
            )
            if externally_shared_with:
                ctx["questions_externally_shared_with"].append(
                    (question, externally_shared_with)
                )

        ctx["additional_terms"] = [
            LazyI18nString(t) for t in self.request.event.settings.additional_terms
        ]
        return ctx


class DisableViaTerms(EventPermissionRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        self.request.event.settings.set("via_terms_enabled", False)
        self.request.event.cache.clear()
        messages.success(self.request, _("Terms and conditions have been disabled."))
        return HttpResponseRedirect(
            reverse(
                "plugins:pretix_via:event.settings.via_terms",
                kwargs={
                    "event": request.event.slug,
                    "organizer": request.organizer.slug,
                },
            )
        )
