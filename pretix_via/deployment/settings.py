# We use these custom settings to be able to use a custom EmailBackend that
# contains a small fix to be able to use Google's smtp relay instead of username/password
# authentication.
# Ref: https://gitlab.com/studieverenigingvia/ict/pretix-via/-/issues/45
EMAIL_BACKEND = EMAIL_CUSTOM_SMTP_BACKEND = "pretix_via.smtp.SMTPRelayFixEmailBackend"
EMAIL_FQDN = config.get("mail", "fqdn", fallback="svia.nl")  # noqa[F821]
