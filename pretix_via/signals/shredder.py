from django.dispatch import receiver

from pretix.base.signals import register_data_shredders
from pretix_via.shredder import OrderViaUserShredder, WaitinglistViaUserShredder


@receiver(register_data_shredders, dispatch_uid="pretix_via_data_shredders")
def register_shredder(sender, **kwargs):
    return [OrderViaUserShredder, WaitinglistViaUserShredder]
