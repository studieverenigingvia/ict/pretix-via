from collections import OrderedDict

from django import forms
from django.dispatch import receiver
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from pretix.base.models import Organizer
from pretix.base.signals import register_global_settings, api_event_settings_fields
from pretix.control.forms.widgets import Select2

from rest_framework import serializers


@receiver(register_global_settings, dispatch_uid="pretix_via_global_settings")
def register_global_settings(sender, **kwargs):
    return OrderedDict(
        [
            (
                "via_oauth_organizer",
                forms.ModelChoiceField(
                    label=_("Login with via: team organizer"),
                    queryset=Organizer.objects.all(),
                    required=True,
                    initial=0,
                    widget=Select2(
                        attrs={
                            "data-model-select2": "generic",
                            "data-select2-url": reverse_lazy(
                                "control:organizers.select2"
                            ),
                            "data-placeholder": _("All organizers"),
                        }
                    ),
                ),
            ),
            (
                "via_oauth_host",
                forms.CharField(
                    label=_("Login with via: Host"),
                    required=True,
                    initial="https://svia.nl/",
                ),
            ),
            (
                "via_oauth_client_id",
                forms.CharField(
                    label=_("Login with via: Client ID"),
                    required=True,
                ),
            ),
            (
                "via_oauth_client_secret",
                forms.CharField(
                    label=_("Login with via: Client secret"),
                    required=True,
                ),
            ),
        ]
    )


@receiver(
    api_event_settings_fields, dispatch_uid="pretix_via_api_event_settings_fields"
)
def pretix_via_api_event_settings_fields(sender, **kwargs):
    return OrderedDict(
        [
            (
                "via_favourer_allowed",
                serializers.BooleanField(allow_null=False, read_only=True),
            ),
            (
                "via_membership_required",
                serializers.BooleanField(allow_null=False, read_only=True),
            ),
        ]
    )
