from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from pretix.base.timemachine import time_machine_now
from pretix.control.signals import event_dashboard_widgets

from pretix_via.shredder import is_event_shredded


@receiver(signal=event_dashboard_widgets)
def live_issues_present(sender, **kwargs):
    return [
        {
            "display_size": "small",
            "priority": 999,
            "content": '<div class="shopstate">{t1}<br><span class="{cls}"><span class="fa {icon}"></span> {state}</span>{t2}</div>'.format(
                t1=_("Your ticket shop has"),
                t2=_("Click here to view"),
                state=_("live issues") if sender.live_issues else _("no live issues"),
                icon="fa-times-circle" if sender.live_issues else "fa-check-circle",
                # Copy the classes from the live dashboard widget for the colors.
                cls="off" if sender.live_issues else "live",
            ),
            "url": reverse(
                "control:event.live",
                kwargs={"event": sender.slug, "organizer": sender.organizer.slug},
            ),
        }
    ]


@receiver(signal=event_dashboard_widgets)
def is_shredded_widget(sender, **kwargs):
    if sender.date_to and sender.date_to > time_machine_now():
        return []

    shredded = is_event_shredded(sender)

    data = {
        "display_size": "small",
        "priority": 999,
        "content": '<div class="shopstate">{t1}<br><span class="{cls}"><span class="fa {icon}"></span> {state}</span>{t2}</div>'.format(
            t1=_("Your ticket shop has been"),
            t2=_("Click here to view") if not shredded else "",
            state=_("shredded") if shredded else _("not shredded yet"),
            icon="fa-times-circle" if not shredded else "fa-check-circle",
            cls="off" if not shredded else "live",
        ),
    }

    if not shredded:
        data["url"] = reverse(
            "control:event.dangerzone",
            kwargs={
                "organizer": sender.organizer.slug,
                "event": sender.slug,
            },
        )

    return [data]
