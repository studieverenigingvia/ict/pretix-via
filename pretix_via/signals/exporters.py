from collections import OrderedDict

from django import forms
from django.dispatch import receiver
from django.utils.translation import gettext as _, gettext_lazy


from pretix.base.exporter import ListExporter
from pretix.base.models import OrderPosition, Order
from pretix.base.signals import register_data_exporters


class OrderViaUserExporter(ListExporter):
    identifier = "viauser"
    verbose_name = gettext_lazy("VIA Membership details export")

    def iterate_list(self, form_data):
        qs = Order.objects.filter(
            event=self.event, status__in=form_data["status"]
        ).select_related("via_user")

        yield self.ProgressSetTotal(total=qs.count() + 1)
        # Column names
        yield [
            _("Order Code"),
            _("Order status"),
            _("E-mailadress"),
            _("Address"),
            _("City"),
            _("Zip"),
            _("Member"),
            _("Favourer"),
            _("First Name"),
            _("Last Name"),
            _("Date of birth"),
        ]

        status_label_lookup = dict(Order.STATUS_CHOICE)

        for order in qs:
            try:
                yield [
                    order.code,
                    status_label_lookup[order.status],
                    order.via_user.email,
                    order.via_user.address,
                    order.via_user.city,
                    order.via_user.zip,
                    order.via_user.member,
                    order.via_user.favourer,
                    order.via_user.first_name,
                    order.via_user.last_name,
                    order.via_user.birth_date,
                ]
            except Order.via_user.RelatedObjectDoesNotExist:
                yield []

    @property
    def additional_form_fields(self) -> dict:
        f = super().additional_form_fields
        f.update(
            OrderedDict(
                [
                    (
                        "status",
                        forms.MultipleChoiceField(
                            label=_("Filter by status"),
                            initial=[Order.STATUS_PAID],
                            choices=Order.STATUS_CHOICE,
                            widget=forms.CheckboxSelectMultiple,
                            required=False,
                        ),
                    ),
                ]
            )
        )
        return f

    def get_filename(self):
        return f"pretix_via_membership_export_{self.event.slug}"


class HeliosExporter(ListExporter):
    identifier = "helios"
    verbose_name = gettext_lazy("Helios export")

    @property
    def additional_form_fields(self):
        return OrderedDict(
            [
                (
                    "authorizing",
                    forms.ModelChoiceField(
                        queryset=self.event.items.all(),
                        label=gettext_lazy("Authorizing product"),
                        widget=forms.RadioSelect(attrs={"class": "scrolling-choice"}),
                        initial=self.event.items.last(),
                    ),
                ),
                (
                    "authorizing_name",
                    forms.ModelChoiceField(
                        queryset=self.event.questions.all(),
                        label=gettext_lazy("Authorizing name"),
                        widget=forms.RadioSelect(attrs={"class": "scrolling-choice"}),
                        initial=self.event.questions.first(),
                    ),
                ),
                (
                    "authorizing_email",
                    forms.ModelChoiceField(
                        queryset=self.event.questions.all(),
                        label=gettext_lazy("Authorizing e-mail"),
                        widget=forms.RadioSelect(attrs={"class": "scrolling-choice"}),
                        initial=self.event.questions.last(),
                    ),
                ),
            ]
        )

    def iterate_list(self, form_data):
        qs = (
            OrderPosition.objects.filter(order__event=self.event)
            .prefetch_related(
                "answers",
            )
            .select_related(
                "order",
                "item",
            )
        )

        qs = qs.filter(order__status=Order.STATUS_PAID)

        yield self.ProgressSetTotal(total=qs.count())

        for op in qs:
            if op.item_id == form_data["authorizing"]:
                answers = {a.question_id: a.answer for a in op.answers.all()}
                authorizing_name = answers.get(form_data["authorizing_name"], "UNKNOWN")
                authorizing_email = answers.get(
                    form_data["authorizing_email"], "UNKNOWN"
                )

                #  The password field indicates a "voter type" where voters
                #  receive an email with a password they can use to vote.
                yield [
                    "password",
                    op.order.code,
                    authorizing_email,
                    f"{authorizing_name} authorized by {op.attendee_name}",
                ]
                continue
            else:
                #  The password field indicates a "voter type" where voters
                #  receive an email with a password they can use to vote.
                yield [
                    "password",
                    op.order.code,
                    op.attendee_email or op.order.email or "",
                    op.attendee_name,
                ]

    def get_filename(self):
        return f"helios_export_{self.event.slug}"


@receiver(
    register_data_exporters, dispatch_uid="pretix_via_register_data_exporters_helios"
)
def register_data_exporters_helios(sender, *args, **kwargs):
    return HeliosExporter


@receiver(
    register_data_exporters, dispatch_uid="pretix_via_register_data_exporters_viauser"
)
def register_data_exporters_viauser(sender, *args, **kwargs):
    return OrderViaUserExporter
