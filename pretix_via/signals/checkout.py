import json
from datetime import date
from typing import Optional

import phonenumbers
from django.dispatch import receiver
from django.http import HttpRequest
from django.template.loader import get_template
from django.utils.translation import gettext_lazy as _

from pretix.base.models import Event, Order
from pretix.base.signals import order_placed
from pretix.control.signals import order_info as control_order_info
from pretix.multidomain.urlreverse import eventreverse
from pretix.presale.signals import (
    checkout_flow_steps,
    order_meta_from_request,
    checkout_confirm_page_content,
    order_info,
    checkout_confirm_messages,
    contact_form_fields_overrides,
    question_form_fields_overrides,
)
from pretix.presale.views.cart import cart_session
from pretix_via.checkoutflow import ViaductUserDataStep
from pretix_via.models import OrderViaUser


@receiver(signal=checkout_flow_steps, dispatch_uid="pretix_via_checkout_step")
def signal_checkout_flow_steps(sender, **kwargs):
    return ViaductUserDataStep


@receiver(order_meta_from_request, dispatch_uid="pretix_via_order_meta")
def order_meta_signal(sender: Event, request: HttpRequest, **kwargs):
    cs = cart_session(request)
    viaduct_user = cs.get("user_info", {})
    return {"user_info": viaduct_user}


@receiver(checkout_confirm_page_content, dispatch_uid="pretix_via_confirm")
def confirm_page(sender: Event, request: HttpRequest, **kwargs):
    template = get_template("pretix_via/checkout_confirm.html")
    cs = cart_session(request)

    birth_date = cs.get("user_info", {}).get("birth_date")

    ctx = {
        "request": request,
        "email": cs.get("user_info", {}).get("email"),
        "has_paid": cs.get("user_info", {}).get("has_paid"),
        "address": cs.get("user_info", {}).get("address"),
        "city": cs.get("user_info", {}).get("city"),
        "zip": cs.get("user_info", {}).get("zip"),
        "first_name": cs.get("user_info", {}).get("first_name"),
        "last_name": cs.get("user_info", {}).get("last_name"),
        "birth_date": date.fromisoformat(birth_date)
        if birth_date is not None
        else None,
    }
    return template.render(ctx)


@receiver(order_info, dispatch_uid="pretix_via_order_info")
def order_info(sender: Event, order: Order, **kwargs):
    template = get_template("pretix_via/order_info.html")

    ctx = {
        "order": order,
        "event": sender,
    }

    return template.render(ctx)


@receiver(control_order_info, dispatch_uid="pretix_via_control_order_info")
def control_order_info(sender: Event, request, order: Order, **kwargs):
    template = get_template("pretix_via/control_order_info.html")

    ctx = {
        "order": order,
        "event": sender,
        "request": request,
    }
    try:
        ctx["via_user"] = order.via_user
    except OrderViaUser.DoesNotExist:
        pass

    return template.render(ctx)


@receiver(order_placed, dispatch_uid="pretix_via_order_placed")
def placed_order(sender: Event, order: Order, **kwargs):
    if order.meta_info_data and order.meta_info_data.get("user_info"):
        # Convert the meta_info to the actual OrderViaUser table.
        viaduct_user = order.meta_info_data.pop("user_info")
        order.meta_info = json.dumps(order.meta_info_data)

        birth_date = viaduct_user.get("birth_date")
        order_via_user = OrderViaUser(
            order=order,
            user_id=viaduct_user.get("id"),
            email=viaduct_user.get("email"),
            address=viaduct_user.get("address"),
            city=viaduct_user.get("city"),
            zip=viaduct_user.get("zip"),
            member=viaduct_user.get("member"),
            favourer=viaduct_user.get("favourer"),
            first_name=viaduct_user.get("first_name"),
            last_name=viaduct_user.get("last_name"),
            birth_date=date.fromisoformat(birth_date)
            if birth_date is not None
            else None,
        )
        order_via_user.save()
        order.viaduct_user = order_via_user
        order.save()


def internationalize_phone_nr(phone_raw: str) -> Optional[str]:
    try:
        region = None
        if not phone_raw.startswith("+") or phone_raw.startswith("00"):
            region = phonenumbers.region_code_for_country_code(31)
        phone = phonenumbers.parse(phone_raw, region=region)
    except phonenumbers.NumberParseException:
        return None

    return phonenumbers.format_number(phone, phonenumbers.PhoneNumberFormat.E164)


@receiver(
    contact_form_fields_overrides,
    dispatch_uid="pretix_via_contact_form_fields_overrides",
)
def checkout_contact_form_fields_overrides(request, order, *args, **kwargs):
    cart = cart_session(request)

    address = cart.get("user_info", {}).get("address", "")
    city = cart.get("user_info", {}).get("city", "")
    country_raw = cart.get("user_info", {}).get("country", "")
    country = None
    if country_raw.lower() in ["netherlands", "nederland", "nl"]:
        country = "NL"
    email = cart.get("user_info", {}).get("email", "")
    first_name = cart.get("user_info", {}).get("first_name", "")
    last_name = cart.get("user_info", {}).get("last_name", "")
    phone_raw = cart.get("user_info", {}).get("phone_nr", "")
    zipcode = cart.get("user_info", {}).get("zip", "")
    phone = internationalize_phone_nr(phone_raw)

    return {
        "name_parts": {
            "initial": {
                "full_name": f"{first_name} {last_name}",
                "calling_name": first_name,
                "given_name": first_name,
                "family_name": last_name,
            },
            "disabled": bool(first_name) or bool(last_name),
        },
        "email": {"initial": email, "disabled": bool(email)},
        "phone": {
            "initial": phone,
            "disabled": bool(phone),
        },
        "street": {"initial": address, "disabled": bool(address)},
        "zipcode": {"initial": zipcode, "disabled": bool(zipcode)},
        "city": {"initial": city, "disabled": bool(city)},
        "country": {"initial": country, "disabled": bool(country)},
    }


@receiver(
    question_form_fields_overrides,
    dispatch_uid="pretix_via_question_form_fields_overrides",
)
def checkout_question_form_fields_overrides(request, **kwargs):
    cart = cart_session(request)

    first_name = cart.get("user_info", {}).get("first_name", "")
    last_name = cart.get("user_info", {}).get("last_name", "")
    email = cart.get("user_info", {}).get("email", "")
    address = cart.get("user_info", {}).get("address", "")
    zipcode = cart.get("user_info", {}).get("zip", "")
    city = cart.get("user_info", {}).get("city", "")
    country = cart.get("user_info", {}).get("country", "")

    # Initial values based on available name schemes in settings
    return {
        "attendee_name_parts": {
            "initial": {
                "full_name": f"{first_name} {last_name}",
                "calling_name": first_name,
                "given_name": first_name,
                "family_name": last_name,
            },
            "disabled": bool(first_name) or bool(last_name),
        },
        "attendee_email": {"initial": email, "disabled": bool(email)},
        "street": {"initial": address, "disabled": bool(address)},
        "zipcode": {"initial": zipcode, "disabled": bool(zipcode)},
        "city": {"initial": city, "disabled": bool(city)},
        "country": {"initial": country, "disabled": bool(country)},
    }


@receiver(checkout_confirm_messages, dispatch_uid="pretix_via_confirm_messages")
def confirm_messages(sender, *args, **kwargs):
    via_terms_enabled = sender.settings.get("via_terms_enabled")
    if not via_terms_enabled:
        return {}
    return {
        "pages": _("I have read and agree with the {url}.").format(
            url='<a href="{url}" target="blank">{text}</a>'.format(
                text=_("Terms and conditions"),
                url=eventreverse(sender, "plugins:pretix_via:terms"),
            )
        )
    }
