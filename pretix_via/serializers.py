from rest_framework import serializers

from pretix.base.models import Event, Order
from pretix_via.models import OrderViaUser, TeamViaGroup


class OrderViaUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderViaUser
        fields = [
            "user_id",
            "email",
            "address",
            "city",
            "zip",
            "member",
            "favourer",
            "first_name",
            "last_name",
            "birth_date",
        ]
        extra_kwargs = {
            "address": {"allow_blank": True},
            "city": {"allow_blank": True},
            "zip": {"allow_blank": True},
        }


class _EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = [
            "name",
            "slug",
        ]


class TeamViaGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamViaGroup
        fields = ["group_id", "team"]
        read_only_fields = ["team"]


class OrderSerializer(serializers.ModelSerializer):
    via_user = OrderViaUserSerializer()
    event = _EventSerializer()

    class Meta:
        model = Order
        fields = ["code", "status", "secret", "event", "via_user"]
