import logging

from django.utils.translation import gettext_noop
from i18nfield.strings import LazyI18nString

from pretix.settings import CONFIG_FILE

logger = logging.getLogger(__name__)

NEW_MAIL_DEFAULTS = {
    "mail_text_signature": {
        "type": LazyI18nString,
        "default": LazyI18nString.from_gettext(
            gettext_noop(
                """Vereniging Informatiewetenschappen Amsterdam (**via**)
**T** (020) 525 7880
**E** [bestuur@svia.nl](mailto:bestuur@svia.nl)
**W** [svia.nl](https://svia.nl/)
"""
            )
        ),
    },
}

mollie_key = CONFIG_FILE.get("pretix_via", "mollie_api_key", fallback="")
logger.info(f"Mollie key '{mollie_key}' default setting.")

DEFAULTS = {
    "via_membership_required": {"default": "True", "type": bool},
    "via_favourer_allowed": {"default": "True", "type": bool},
    "primary_color": {"default": "#304BA3", "type": str},
    "invoice_address_asked": {"default": "False", "type": bool},
    "waiting_list_enabled": {"default": "True", "type": bool},
    "payment_giftcard__enabled": {"default": "False", "type": bool},
    "payment_mollie__enabled": {"default": "True", "type": bool},
    "payment_mollie_method_ideal": {"default": "True", "type": bool},
    "attendee_names_asked": {"default": "True", "type": bool},
    "ticket_download": {"default": "True", "type": bool},
    "ticketoutput_pdf__enabled": {"default": "True", "type": bool},
    "payment_mollie_api_key": {"default": mollie_key, "type": str},
    "max_items_per_order": {"default": "1", "type": int},
}
