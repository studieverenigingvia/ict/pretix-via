from django.urls import path, re_path

from pretix.api.urls import orga_router, event_router
from pretix.api.views.event import EventViewSet
from pretix.presale.views.waiting import WaitingView
from pretix_via import unshredded_event_list
from pretix_via.filters import NameFilteringEventFilter
from pretix_via.waitinglist import ViaUserAwareWaitingListForm
from . import views

urlpatterns = [
    re_path(
        r"^control/event/(?P<organizer>[^/]+)/(?P<event>[^/]+)/settings/via/$",
        views.EventViaSettings.as_view(),
        name="event.settings.via",
    ),
    re_path(
        r"^control/event/(?P<organizer>[^/]+)/(?P<event>[^/]+)/settings/via/terms$",
        views.EventViaTermsSettings.as_view(),
        name="event.settings.via_terms",
    ),
    re_path(
        r"^control/event/(?P<organizer>[^/]+)/(?P<event>[^/]+)/settings/via/terms/disable$",
        views.DisableViaTerms.as_view(),
        name="event.settings.via_terms.disable",
    ),
    # A better name would be something like 'unshredded-events', but the pretix navbar highlights events, if there is a
    # 'events' in the url, so we have to something else here.
    re_path(
        r"^control/unshredded-events/$",
        unshredded_event_list.UnshreddedEventList.as_view(),
        name="unshredded-list",
    ),
]

event_patterns = [
    path("terms-and-conditions/", views.TermsConditionsView.as_view(), name="terms")
]

# Get all order under the organizer.
orga_router.register(
    r"via_users/(?P<via_user>[0-9]+)/orders",
    views.ViaUserOrdersListView,
    "viauserorder",
)

orga_router.register(r"via_teams", views.TeamViaGroupListView, "viateams")

# Get all order under the organizer (for this event).
event_router.register(
    r"via_users/(?P<via_user>[0-9]+)/orders",
    views.ViaUserEventOrdersListView,
    "eventviauser",
)

# Get the via user for a specific order.
event_router.register(r"orders", views.OrderViaUserRetrieveAPIView, "orderviauser")

# Update via group of existing team.
orga_router.register(r"teams", views.TeamViaGroupUpdateAPIView, "teams-via")

EventViewSet.filterset_class = NameFilteringEventFilter
WaitingView.form_class = ViaUserAwareWaitingListForm
WaitingView.get_form_kwargs = ViaUserAwareWaitingListForm.get_form_kwargs_decorator(
    WaitingView.get_form_kwargs
)
WaitingView.form_valid = ViaUserAwareWaitingListForm.form_valid_decorator(
    WaitingView.form_valid
)
