import logging
import typing
from decimal import Decimal
from . import __version__

from django.apps import AppConfig
from django.utils.translation import gettext_lazy

from pretix_via.settings import DEFAULTS

if typing.TYPE_CHECKING:
    from pretix.base.models import Event


logger = logging.getLogger(__name__)


class PluginApp(AppConfig):
    name = "pretix_via"
    verbose_name = "Pretix for via"

    class PretixPluginMeta:
        name = gettext_lazy("Pretix for via")
        author = "Maico Timmerman"
        description = gettext_lazy(
            "Study association via membership checker for pretix"
        )
        visible = True
        version = __version__
        # compatibility = "pretix>=2.7.0"

    def ready(self):
        from .signals import (  # noqa: F401
            checkout,
            live_issues,
            navigation,
            settings,
            shredder,
            dashboard,
            exporters,
            tickets,
        )
        from pretix.base.settings import settings_hierarkey
        from .settings import NEW_MAIL_DEFAULTS

        # Based on pretix.base.settings line 851.
        for k, v in NEW_MAIL_DEFAULTS.items():
            settings_hierarkey.add_default(k, v["default"], v["type"])
            logger.info(f"Overwritten '{k}' default mail setting.")

        for k, v in DEFAULTS.items():
            settings_hierarkey.add_default(k, v["default"], v["type"])
            logger.info(f"Adding '{k}' default setting.")

        from pretix.control.views.main import EventWizard

        assert (
            EventWizard.templates["foundation"]
            == "pretixcontrol/events/create_foundation.html"
        ), "Original template expected at pretixcontrol/events/create_foundation.html"
        EventWizard.templates["foundation"] = "pretix_via/create_foundation.html"

    def installed(self, event: "Event", **kwargs):
        from pretix.base.models import TaxRule

        TaxRule.objects.filter(event=event).get_or_create(
            event=event,
            name={"en": "VAT", "nl": "btw"},
            rate=Decimal("21.00"),
            price_includes_tax=True,
        )
        TaxRule.objects.filter(event=event).get_or_create(
            event=event,
            name={"en": "VAT", "nl": "btw"},
            rate=Decimal("9.00"),
            price_includes_tax=True,
        )

        from .signals.tickets import create_via_ticket_layout

        create_via_ticket_layout(event)

        logger.info("Enabled for event %s", event.name)


logger.info("Loaded pretix_via configurations")
default_app_config = "pretix_via.PluginApp"
