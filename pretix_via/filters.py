from django.db.models import Q
from django_filters import BaseInFilter, NumberFilter, CharFilter
from django_filters.rest_framework import FilterSet
from django_scopes import scopes_disabled

from pretix.api.views.event import EventFilter
from pretix.base.models import Team, Order


class NumberInFilter(BaseInFilter, NumberFilter):
    pass


class CharInFilter(BaseInFilter, CharFilter):
    pass


with scopes_disabled():

    class NameFilteringEventFilter(EventFilter):
        import django_filters

        group_id = NumberInFilter(method="group_id_in")

        def group_id_in(self, queryset, name, value):
            teams = Team.objects.filter(via_group__group_id__in=value).values_list(
                "id", flat=True
            )
            return queryset.filter(
                Q(id__in=teams.values_list("limit_events__id", flat=True))
            )


with scopes_disabled():

    class OrderFilter(FilterSet):
        status__in = CharInFilter(field_name="status", lookup_expr="in")

        class Meta:
            model = Order
            fields = ["status"]
