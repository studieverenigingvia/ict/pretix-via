import logging
from datetime import date
from typing import Optional

from django.contrib import messages
from django.db.transaction import atomic
from django.shortcuts import redirect
from django.utils.translation import pgettext_lazy, gettext as _

from pretix.base.models import Order
from pretix.base.services.orders import OrderError
from pretix.base.settings import GlobalSettingsObject
from pretix.presale.checkoutflow import TemplateFlowStep
from pretix.presale.views import CartMixin
from pretix_via import api
from pretix_via.models import WaitingListEntryViaUser

from pretix_via.models import OrderViaUser

_logger = logging.getLogger(__name__)


class ViaductUserDataStep(CartMixin, TemplateFlowStep):
    priority = 1

    identifier = "viaduct"
    template_name = "pretix_via/checkout_viaduct_user_data.html"
    icon = "group"
    label = pgettext_lazy("checkoutflow", "VIA Membership")

    def __init__(self, event):
        super().__init__(event)

        gs = GlobalSettingsObject()
        self.client_id = gs.settings.get("via_oauth_client_id")
        self.client_secret = gs.settings.get("via_oauth_client_secret")
        self.host = gs.settings.get("via_oauth_host", "https://svia.nl/")

    def get(self, request):
        self.request = request
        voucher_users = self.get_voucher_users()
        if not self.widget_viaduct_url and not voucher_users:
            _logger.warning(
                "Widget data: %s, voucher_users: %s",
                self.cart_session.get("widget_data", {}),
                voucher_users,
            )
            messages.error(
                self.request,
                _(
                    "Unable to retrieve membership data. Please refresh the via-site and try again."
                ),
            )
            return self.render()

        if len({u.user_id for u in voucher_users}) > 1:
            messages.error(
                self.request,
                _(
                    "You have used waiting list vouchers of multiple via users in your cart."
                ),
            )
            return self.render()

        if not self.viaduct_user:
            messages.error(
                self.request,
                _(
                    "Your membership information is out of date, please refresh the page."
                ),
            )
            return self.render()

        if self.existing_order(self.viaduct_user):
            messages.error(self.request, _("You already have an order for this event."))
            return self.render()

        if not self.via_user_can_order_ticket():
            if self.request.event.settings.get("via_favourer_allowed"):
                messages.error(
                    self.request, _("Only via members or favourer can buy tickets.")
                )
            else:
                messages.error(self.request, _("Only via members can buy tickets."))

        return self.render()

    def existing_order(self, viaduct_user) -> Optional[OrderViaUser]:
        existing_order = OrderViaUser.objects.filter(
            order__event=self.request.event, user_id=viaduct_user.get("id")
        ).first()
        if existing_order and existing_order.order.status in (
            Order.STATUS_PAID,
            Order.STATUS_PENDING,
        ):
            return existing_order
        return None

    @atomic
    def post(self, request):
        self.request = request
        if not self.viaduct_user:
            raise OrderError(_("You can only use this shop on the via-site"))

        return redirect(self.get_next_url(request))

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx["cart"] = self.get_cart()
        ctx["viaduct_user"] = self.viaduct_user
        ctx["is_completed"] = self.is_completed(self.request)
        ctx["email"] = self.viaduct_user.get("email")
        ctx["address"] = self.viaduct_user.get("address")
        ctx["city"] = self.viaduct_user.get("city")
        ctx["zip"] = self.viaduct_user.get("zip")
        ctx["first_name"] = self.viaduct_user.get("first_name")
        ctx["last_name"] = self.viaduct_user.get("last_name")
        birth_date = self.viaduct_user.get("birth_date")
        ctx["birth_date"] = (
            date.fromisoformat(birth_date) if birth_date is not None else None
        )
        return ctx

    def via_user_can_order_ticket(self):
        if not self.viaduct_user:
            return False

        if self.existing_order(self.viaduct_user):
            return False

        is_member = self.viaduct_user.get("member", False)
        is_favourer = self.viaduct_user.get("favourer", False)
        member_required = self.request.event.settings.get("via_membership_required")
        favourer_allowed = self.request.event.settings.get("via_favourer_allowed")
        if member_required and is_member:
            return True
        elif member_required and favourer_allowed and is_favourer:
            return True
        return False

    def get_voucher_users(self):
        cart = self.get_cart()
        voucher_ids = [cp.id for cp in cart["positions"] if cp.voucher]
        return WaitingListEntryViaUser.objects.filter(
            waitinglistentry__voucher__cartposition__in=voucher_ids
        )

    @property
    def voucher_user(self):
        voucher_users = self.get_voucher_users()
        if not voucher_users or len({u.user_id for u in voucher_users}) > 1:
            return {}

        via_user = voucher_users.first()
        return {
            "id": via_user.user_id,
            "email": via_user.email,
            "address": via_user.address,
            "city": via_user.city,
            "zip": via_user.zip,
            "member": via_user.member,
            "favourer": via_user.favourer,
            "first_name": via_user.first_name,
            "last_name": via_user.last_name,
            # Explicitly convert back to isoformat-string, as the rest of the flow
            # assumes the response from this function is the same
            # as the API response.
            "birth_date": via_user.birth_date.isoformat()
            if via_user.birth_date is not None
            else None,
        }

    @property
    def widget_viaduct_url(self):
        widget_data = self.cart_session.get("widget_data", {})
        return widget_data.get("user-info-url")

    @property
    def viaduct_url(self):
        return self.cart_session.get("user_info_url")

    @viaduct_url.setter
    def viaduct_url(self, value):
        self.cart_session["user_info_url"] = value

    @property
    def viaduct_user(self):
        if (
            self.widget_viaduct_url
            and self.viaduct_url
            and self.widget_viaduct_url == self.viaduct_url
        ):
            return self.cart_session.get("user_info", {})

        if self.widget_viaduct_url:
            response = api.get_via_user_details(
                self.host, self.client_id, self.client_secret, self.widget_viaduct_url
            )
            if response:
                self.cart_session["user_info"] = response
                self.viaduct_url = self.widget_viaduct_url
                return self.cart_session.get("user_info", {})

        if self.voucher_user:
            self.cart_session["user_info"] = self.voucher_user
            return self.cart_session.get("user_info", {})

        return self.cart_session.get("user_info", {})

    def is_completed(self, request, warn=False):
        self.request = request
        if not self.viaduct_user:
            return False

        if self.existing_order(self.viaduct_user):
            return False
        return True

    def is_applicable(self, request):
        return not request.event.testmode
